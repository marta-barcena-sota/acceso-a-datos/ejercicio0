<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Puerto;

/**
 * PuertoSearch represents the model behind the search form of `app\models\Puerto`.
 */
class PuertoSearch extends Puerto
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nompuerto', 'categoria'], 'safe'],
            [['altura', 'numetapa', 'dorsal'], 'integer'],
            [['pendiente'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Puerto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'altura' => $this->altura,
            'pendiente' => $this->pendiente,
            'numetapa' => $this->numetapa,
            'dorsal' => $this->dorsal,
        ]);

        $query->andFilterWhere(['like', 'nompuerto', $this->nompuerto])
            ->andFilterWhere(['like', 'categoria', $this->categoria]);

        return $dataProvider;
    }
}

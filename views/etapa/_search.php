<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EtapaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="etapa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'numetapa') ?>

    <?= $form->field($model, 'kms') ?>

    <?= $form->field($model, 'salida') ?>

    <?= $form->field($model, 'llegada') ?>

    <?= $form->field($model, 'dorsal') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

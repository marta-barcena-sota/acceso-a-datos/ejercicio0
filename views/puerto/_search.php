<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PuertoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="puerto-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'nompuerto') ?>

    <?= $form->field($model, 'altura') ?>

    <?= $form->field($model, 'categoria') ?>

    <?= $form->field($model, 'pendiente') ?>

    <?= $form->field($model, 'numetapa') ?>

    <?php // echo $form->field($model, 'dorsal') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
